<script src="js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.js"></script>
<!-- App -->
<script src="js/app.js"></script>  
<script src="js/slimscroll/jquery.slimscroll.min.js"></script>
<script src="js/charts/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="js/charts/sparkline/jquery.sparkline.min.js"></script>
<script src="js/charts/flot/jquery.flot.min.js"></script>
<script src="js/charts/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/charts/flot/jquery.flot.spline.js"></script>
<script src="js/charts/flot/jquery.flot.pie.min.js"></script>
<script src="js/charts/flot/jquery.flot.resize.js"></script>
<script src="js/charts/flot/jquery.flot.grow.js"></script>

<script src="js/charts/flot/demo.js"></script>

<script src="js/datatables/jquery.dataTables.min.js"></script> 
<script src="js/datatables/jquery.csv-0.71.min.js"></script>
<script src="js/datatables/demo.js"></script>

<script src="js/calendar/bootstrap_calendar.js"></script>
<script src="js/calendar/demo.js"></script>

<script src="js/sortable/jquery.sortable.js"></script>
<script src="js/app.plugin.js"></script>
<script src="js/alertify.js"></script>
<script type="text/javascript" language="javascript" src="js/tinymce/tinymce.min.js"></script>
<script src="js/datepicker/bootstrap-datepicker.js"></script>
<script src="js/controller.js"></script>