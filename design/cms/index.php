<!DOCTYPE html>
<html lang="en" class="app">
<?php include "includes/head_login.php"; ?>
<body class="">
  <section id="content" class="m-t-lg wrapper-md animated fadeInUp">    
    <div class="container aside-xl">
      <div align="center">
        <a class="logo_login" href="index.php">
          <img src="images/logo_ayo.png" height="150" alt="">
        </a>
      </div>
      <br>
      <section class="m-b-lg">
        <form action="home.php" method="post">
          <div class="list-group">
            <div class="list-group-item">
              <input type="email" placeholder="Email" class="form-control no-border">
            </div>
            <div class="list-group-item">
               <input type="password" placeholder="Password" class="form-control no-border">
            </div>
          </div>
          <button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
        </form>
      </section>
      <div class="logo_login_foot" align="center">
        <img src="images/logo2.png" alt="" height="30"><br><br>
        <div style="font-size: 11px">Copyright of Garuda Indonesia @2017 - Allright reserved</div>

      </div>
    </div>
  </section>
  <!-- footer -->
  <?php include "includes/js_login.php"; ?>
  <!-- / footer -->
  
</body>
</html>