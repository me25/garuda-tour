<!doctype html>	
<html>
<?php include "includes/head.php";?></html>
<body class="body_pop">
<div class="container_pop container_pop_reg">
	<img src="assets/images/btn_close.png" alt="" class="close_pop close_box_in">
	<div class="title_pop">REGISTER</div>
	<div class="notif">
		Akun sudah terdaftar sebelumnya
	</div>
	<form action="register2.php?login=1" method="post" target="">
		<div class="group-input">
			<label>NAMA</label>
			<input type="text" placeholder="Nama" required>
		</div>
		<div class="group-input">
			<label>ALAMAT EMAIL</label>
			<input type="text" placeholder="Email Address" data-validation="email" required>
		</div>
		<div class="group-input">
			<label>PASSWORD</label>
			<input name="password" type="password" data-validation="length" data-validation-length="min8">
		</div>
		<div class="clearfix"></div>
		<div align="center">
			<input type="submit" value="REGISTER" class="btn">
			<div class="clearfix"></div>
			<br>
			Sudah terdaftar sebelumnya?
			<br>
			<a href="login.php">Login Sekarang</a>
		</div>

	</form>
</div>

<?php include "includes/js.php";?>
</body>
</html>