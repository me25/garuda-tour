<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body>
<?php 
	include "includes/header.php";
?>
<!-- s:cover -->
<section id="cover">
	<div id="slide_cover">
		<div class="ratio16_9 box_img">
			<a href="detail.php">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/cover_1.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="detail.php">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/cover_2.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="detail.php">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/cover_3.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="detail.php">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/cover_4.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="detail.php">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/cover_5.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
	</div>
	<img src="assets/images/arrow_left.png" alt="" class="nav_cover nav_left">
	<img src="assets/images/arrow_right.png" alt="" class="nav_cover nav_right">
	<img src="assets/images/logo_5star_color.png" alt="" class="bni_logo">
	<img src="assets/images/bni_pesona.png" class="bni_card box_modal_full" alt="bni_card.php">
	
	<a href="#intro" class="more">
		EXPLORE MORE<br>
		<img src="assets/images/expand more.png" alt="">
	</a>
	
</section>
<!-- e:cover -->
<!-- s:intro -->
<section id="intro" class="section">
	<div class="container">
		<h2 class="title">Penawaran paket liburan terbaik dari Garuda Indonesia Holidays</h2>
		Garuda Indonesia Holidays menyediakan pilihan paket liburan untuk wisata, perjalanan bisnis dan kombinasi untuk perjalanan di udara dan di darat, untuk komunitas dan hobi tertentu, dan petualangan dengan paket ekowisata. Paket wisata baru dan menarik juga telah tersedia, memenuhi berbagai ketertarikan, hobi-hobi, seni & budaya, olahraga dan petualangan, fashion dan kuliner serta kepentingan komunitas tertentu.
	</div>
</section>
<!-- e:intro -->
<!-- s:list -->
<div class="container">
	<article class="list1">
		<div class="box_img ratio_box ">
		
			<div class="img_con lqd">
				<img src="assets/images/img_banyuwangi.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>BANYUWANGI</h2>
			<div class="place">Banyuwangi - Indonesia</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong>Rp.7.370.000,-</strong>
			</div>
			<a href="detail.php" class="more">Lihat Rincian</a>
			
		</div>
	</article>
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="assets/images/img_malang.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>MALANG</h2>
			<div class="place">Malang - Indonesia</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong>Rp.5.825.600,-</strong>
			</div>
			<a href="detail.php" class="more">Lihat Rincian</a>
			
		</div>
	</article>
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="assets/images/img_padang.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>PADANG</h2>
			<div class="place">Padang - Indonesia</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong>Rp.5.746.400,-</strong>
			</div>
			<a href="detail.php" class="more">Lihat Rincian</a>
			
		</div>
	</article>
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="assets/images/img_belitung.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>BELITUNG</h2>
			<div class="place">Belitung - Indonesia</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong>Rp.4.395.600,-</strong>
			</div>
			<a href="detail.php" class="more">Lihat Rincian</a>
			
		</div>
	</article>
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="assets/images/img_labuan.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>LABUAN BAJO</h2>
			<div class="place">Labuan Bajo - Indonesia</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong>Rp.5.746.400-</strong>
			</div>
			<a href="detail.php" class="more">Lihat Rincian</a>
			
		</div>
	</article>
</div>
<div class="clearfix"></div>
<div class="container banner_bottom">
	<a href="#"><img src="assets/images/banner_1000x200.jpg" alt=""></a>
</div>
<!-- e:list -->
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
</body>
</html>