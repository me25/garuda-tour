<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body>
<?php 
	include "includes/header.php";
?>
<div class="nav_order">
	<div class="container">
		<div class="page">2. Informasi Wisatawan</div>
		<div class="order_pos">
			<span class="selected">1. Pilih & Pesan</span>
			<span class="selected">2. Data Wisatawan</span>
			<span class="selected">3. Bayar</span>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container" data-sticky_parent>
	<form action="payment.php" method="post">
	<!-- s:detail_left -->
	<div class="detail_left">
		<h3 class="title3">Informasi Kartu</h3>
		<div class="notif m10">
			Batas waktu pembayaran 10 menit, bila tidak melakukan pembayaran makan pesanan akan di anggap batal.
		</div>
		<div class="box_kontak">
			<img src="assets/images/cc_bni.jpg" alt="" width="100%">
			<div class="text">
				<div class="group-input">
					<label>BNI Credit Card Number</label>
					<input type="text" name="nama" class="credit" required="">
				</div>
				<div class="clearfix"></div>
				<div class="group-input group-input3">
					<label for="">Expiration Date</label>
					<div class="group-input">
						<label>Bulan</label>
						<div class="select-style">
							<select name="lahir-tgl" id="" class="" required="">
								<option value="">Bulan</option>
								<option value="">1</option>
								<option value="">2</option>
								<option value="">3</option>
							</select>
						</div>
					</div>
					<div class="group-input">
						<label>Tahun</label>
						<div class="select-style">
							<select name="lahir-tgl" id="" class="" required="">
								<option value="">Tahun</option>
								<option value="">2017</option>
								<option value="">2018</option>
								<option value="">2019</option>
							</select>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="group-input group-input2 input_security">
					<label>
						Security Code (CVV) 
						<span>
							?
							<div class="cc_back">
								<img src="assets/images/cc_back.png" alt="">
							</div>
						</span>
					</label>
					<input type="text" name="nama" required="">
				</div>
				<div class="clearfix"></div>
				<div class="group-input">
					<label>Cicilan BNI</label>
					<div class="select-style">
						<select name="lahir-tgl" id="" class="" required="">
							<option value="">Full Payment</option>
							<option value="">3</option>
							<option value="">6</option>
							<option value="">12</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<br>
		<h3 class="title3">Billing Informasi</h3>
		<div class="box_kontak">
			<div class="text">
				<div class="group-input">
					<label>Name</label>
					<input type="text" name="nama">
				</div>
				<div class="group-input">
					<label>E-mail</label>
					<input type="text" name="email" data-validation="email" required="">
				</div>
				<div class="group-input">
					<label>Telepon Selular</label>
					<input type="text" name="phone" required="">
				</div>
				
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- e:detail_left -->
	<!-- s:detail_right -->
	<div class="detail_right" data-sticky_column>
		<div class="box_">
			<div class="title2">Pesanan Anda</div>
			
			<div class="text">
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_time.png" alt="">
					</div>
					<label class="input-date">
						<span>Berangkat</span>
						17-07-2017
					</label>
					<label class="input-date">
						<span>Kembali</span>
						19-07-2017
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_dewasa.png" alt="">
					</div>
					<div class="info">Wisatawan</div>
					<div class="input_num">
						<input type='text' name='quantity' value='3' class='qty' disabled/>
					</div>
					<div class="clearfix"></div>
					<ol class="list_nama">
						<li>Rudianto</li>
						<li>Budiono</li>
						<li>Sucipto</li>
					</ol>
				</div>

				
			</div>
			<div class="total total2">
				<div class="num">
					TOTAL
					<strong>Rp.12.000.000,-</strong>
					<span>(1 orang x Rp.2.500.000)</span>
				</div>
				<div class="clearfix"></div>
			</div>
			<div align="center">
				<input type="submit" class="btn_submit" value="KONFIRMASI PEMBAYARAN">
			</div>
		</div>
	</div>
	<!-- e:detail_right -->
	</form>
	<div class="clearfix"></div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
<script type='text/javascript'>
$(window).load(function(){
	var array = ["2017-05-19","2017-05-26","2017-05-12","2017-05-05"]
	  $('.pilihtanggal').datepicker({
	    beforeShowDay: function(date){
	        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
	        return [ array.indexOf(string) >= 0 ]
	    }
	});
});
</script>
</body>
</html>