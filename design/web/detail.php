<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body>
<?php 
	include "includes/header.php";
?>
<div class="detail_cover">
	<img src="assets/images/img_padang.jpg" alt="">
</div>
<div class="container" data-sticky_parent>
	<!-- s:detail_left -->
	<div class="detail_left">
		<h1>3 Days and 2 Nights at Padang</h1>
		<h2>Highlights</h2>
		<div class="desc">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Negat esse eam, inquit, propter se expetendam. Hanc quoque iucunditatem, si vis, transfer in animum; Profectus in exilium Tubulus statim nec respondere ausus</p>
			<p>
			Primum in nostrane potestate est, quid meminerimus? Istam voluptatem, inquit, Epicurus ignorat? Beatum, inquit. Utilitatis causa amicitia est quaesita. Ille incendat? Si quicquam extra virtutem habeatur in bonis. Quodcumque in mentem incideret, et quodcumque tamquam occurreret. Nos paucis ad haec additis finem faciamus aliquando; Cui Tubuli nomen odio non est?</p>
		</div>
		
		<h2>Hotel</h2>
		<div class="desc">
			<p>
			Primum in nostrane potestate est, quid meminerimus? Istam voluptatem, inquit, Epicurus ignorat? Beatum, inquit. Utilitatis causa amicitia est quaesita. Ille incendat? Si quicquam extra virtutem habeatur in bonis. Quodcumque in mentem incideret, et quodcumque tamquam occurreret. Nos paucis ad haec additis finem faciamus aliquando; Cui Tubuli nomen odio non est?</p>
		</div>
		<h2>Galeri</h2>
		<div class="gal_img">
			<a class="box_img ratio4_3 " data-fancybox="images" href="assets/images/padang1.jpg">
				<div class="img_con lqd">
					<img src="assets/images/padang1.jpg" alt="">
				</div>
			</a>
			<a class="box_img ratio4_3 " data-fancybox="images" href="assets/images/padang2.jpg">
				<div class="img_con lqd">
					<img src="assets/images/padang2.jpg" alt="">
				</div>
			</a>
			<a class="box_img ratio4_3 " data-fancybox="images" href="assets/images/padang3.jpg">
				<div class="img_con lqd">
					<img src="assets/images/padang3.jpg" alt="">
				</div>
			</a>
			<a class="box_img ratio4_3 " data-fancybox="images" href="assets/images/padang5.jpg">
				<div class="img_con lqd">
					<img src="assets/images/padang5.jpg" alt="">
				</div>
			</a>
			<a class="box_img ratio4_3 " data-fancybox="images" href="assets/images/padang6.jpg">
				<div class="img_con lqd">
					<img src="assets/images/padang6.jpg" alt="">
				</div>
			</a>
			<a class="box_img ratio4_3 " data-fancybox="images" href="assets/images/padang4.jpg">
				<div class="img_con lqd">
					<img src="assets/images/padang4.jpg" alt="">
				</div>
			</a>
		</div>
		<div class="clearfix"></div>
		<h2>Rencana Perjalanan</h2>
		<div class="list_rp">
			<strong>Hari ke-1</strong>
			<ol>
				<li>Tiba di Bandara Internasional Minangkabau</li>
				<li>Pantai Air Manis</li>
				<li>Menuju Penginapan</li>
				<li>Malam di Bukittinggi</li>
			</ol>
			<strong>Hari ke-2</strong>
			<ol>
				<li>Tiba di Bandara Internasional Minangkabau</li>
				<li>Pantai Air Manis</li>
				<li>Menuju Penginapan</li>
				<li>Malam di Bukittinggi</li>
			</ol>
			<strong>Hari ke-3</strong>
			<ol>
				<li>Tiba di Bandara Internasional Minangkabau</li>
				<li>Pantai Air Manis</li>
				<li>Menuju Penginapan</li>
				<li>Malam di Bukittinggi</li>
			</ol>
		</div>
	</div>
	<!-- e:detail_left -->
	<!-- s:detail_right -->
	<div class="detail_right" data-sticky_column>
		<div class="box_">
			<form action="order.php" method="post">
			<div class="text">
				<div class="title">Paket 3D2N Padang</div>
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_map.png" alt="">
					</div>
					<label class="input-date input-date_full">
						<span>Kota Keberangkatan</span>
						<div class="select-style">
							<select name="title" id="" required="">
								<option value="">Pilih Kota</option>
								<option value="">Jakarta</option>
								<option value="">Makasar</option>
								<option value="">Bali</option>
							</select>
						</div>
					</label>
					
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_time.png" alt="">
					</div>
					<label class="input-date input-date_full">
						<span>Tanggal</span>
						<div class="select-style">
							<select name="title" id="" required="">
								<option value="">Pilih Tanggal</option>
								<option value="">21 Juni 2017 - 24 Juni 2017</option>
								<option value="">25 Juli 2017 - 28 Juli 2017</option>
								<option value="">5 Agustus 2017 - 8 Agustus 2017</option>
							</select>
						</div>
					</label>
					<!-- <label class="input-date">
						<span>Berangkat</span>
						<input type="text" class="pilihtanggal" placeholder="Pilih tanggal" id="from" required>
					</label>
					<label class="input-date">
						<span>Kembali</span>
						<input type="text" disabled id="to">
					</label> -->
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_dewasa.png" alt="">
					</div>
					<div class="info">Wisatawan</div>
					<div class="input_num">
						 <input type='button' value='-' class='qtyminus' field='quantity' />
					    <input type='text' name='quantity' id="quantity" value='1' class='qty' disabled="" required />
					    <input type='button' value='+' class='qtyplus' field='quantity' />
					</div>
					<div class="clearfix"></div>
				</div>
				
			</div>
			<div class="total">
				<div class="num">
					TOTAL
					<strong class="total_cost">Rp.5.746.400,-</strong>
					<span>(1 Wisatawan x Rp.5.746.400)</span>
				</div>
				<div class="num sisa">
					Sisa Kuota
					<strong>25</strong>
					<span>Wisatawan</span>
				</div>
				<div class="clearfix"></div>
			</div>
			<div align="center">
				<input type="submit" class="btn_submit" value="PESAN SEKARANG">
			</div>
			</form>
		</div>
	</div>
	<!-- e:detail_right -->
	<div class="clearfix"></div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
<script type='text/javascript'>

$(function() {
	var array = ["2017-05-19","2017-05-26","2017-05-12","2017-05-05"]
    $( "#from, #to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        beforeShowDay: function(date){
	        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
	        return [ array.indexOf(string) >= 0 ]
	    },
        onSelect: function( selectedDate ) {
            if(this.id == 'from'){
              var dateMin = $('#from').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); 
              //set jumlah hari
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 3); 
			$('#to').val($.datepicker.formatDate('mm-dd-yy', new Date(rMax)));                    
            }
        }
    });

});
</script>
</body>
</html>