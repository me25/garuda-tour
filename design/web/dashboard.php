<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body>
<?php 
	include "includes/header.php";
?>
<div class="container">
	<div class="dashboard">
		<h1>Hello Rudianto</h1>
		rudianto@gmail.com
		<div class="clearfix"></div>
		<div class="link_dash">
			<a href="#" class="selected">Dashboard</a>
			<a href="#" alt="edit_profil.php" class="box_modal_full">Edit Profile</a>
			<a href="#" alt="change_password.php" class="box_modal_full">Change Password</a>
			<a href="#">Logout</a>
		</div>
		<div class="clearfix"></div>
		<h3>Daftar Pesanan</h3>
		<br>
		<div class="box_ box_2 box_dash">
			<div class="text">
				<div class="desti">Paket 3D2N Padang</div>
				<div class="clearfix"></div>
				<div class="info_total info_code">
					Kode Pesanan
					<strong>A2DFE</strong>
				</div>
				<div class="group-input info_time">
					<div class="ico">
						<img src="assets/images/ico_time.png" alt="">
					</div>
					<label class="input-date">
						<span>Berangkat</span>
						17-07-2017
					</label>
					<label class="input-date">
						<span>Kembali</span>
						19-07-2017
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input info_wis">
					<div class="ico">
						<img src="assets/images/ico_dewasa.png" alt="">
					</div>
					<div class="info"> 2 Wisatawan</div>
					<div class="clearfix"></div>
				</div>
				<div class="info_total">
					TOTAL
					<strong>Rp.12.000.000,-</strong>
					<span>(1 orang x Rp.2.500.000)</span>
				</div>
				<div class="info_total status">
					STATUS
					<strong class="paid">Paid</strong>
				</div>
				<div class="action">
					<a href="#" class="box_modal_full btn" alt="order_info.php">Lihat Rincian</a>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="box_ box_2 box_dash">
			<div class="text">
				<div class="desti">Paket 3D2N Padang</div>
				<div class="clearfix"></div>
				<div class="info_total info_code">
					Kode Pesanan
					<strong>A2DFE</strong>
				</div>
				<div class="group-input info_time">
					<div class="ico">
						<img src="assets/images/ico_time.png" alt="">
					</div>
					<label class="input-date">
						<span>Berangkat</span>
						17-07-2017
					</label>
					<label class="input-date">
						<span>Kembali</span>
						19-07-2017
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input info_wis">
					<div class="ico">
						<img src="assets/images/ico_dewasa.png" alt="">
					</div>
					<div class="info"> 2 Wisatawan</div>
					<div class="clearfix"></div>
				</div>
				<div class="info_total">
					TOTAL
					<strong>Rp.12.000.000,-</strong>
					<span>(1 orang x Rp.2.500.000)</span>
				</div>
				<div class="info_total status">
					STATUS
					<strong class="paid unpaid">Unpaid</strong>
				</div>
				<div class="action">
					<a href="#" class="box_modal_full btn" alt="order_info.php">Lihat Rincian</a>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
</body>
</html>