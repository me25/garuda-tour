<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body>
<?php 
	include "includes/header.php";
?>
<div class="blue_block">
	<div class="container container2">
		<h1>Selamat! Pesanan Anda Telah Kami Terima!</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Negat esse eam, inquit, propter se expetendam. Hanc quoque iucunditatem, si vis, transfer in animum; Profectus in exilium Tubulus statim nec respondere ausus;</p>
		<p>Primum in nostrane potestate est, quid meminerimus? Istam voluptatem, inquit, Epicurus ignorat? Beatum, inquit. Utilitatis causa amicitia est quaesita. Ille incendat? Si quicquam extra virtutem habeatur in bonis. Quodcumque in mentem incideret, et quodcumque tamquam occurreret. Nos paucis ad haec additis finem faciamus aliquando; Cui Tubuli nomen odio non est?</p>
	</div>
</div>
<div class="container container2">
	<h3>RINCIAN PESANAN</h3>
	<br>
	<div class="box_ box_2">
		<div class="text">
			<div class="title">
				Paket 3D2N Padang
				<span class="code">
					KODE PESANAN
					<strong>B46DF</strong>
				</span>
			</div>
			<div class="clearfix"></div>
			<div class="info_box">
				<div class="group-input info_time">
					<div class="ico">
						<img src="assets/images/ico_time.png" alt="">
					</div>
					<label class="input-date">
						<span>Berangkat</span>
						17-07-2017
					</label>
					<label class="input-date">
						<span>Kembali</span>
						19-07-2017
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input info_wis">
					<div class="ico">
						<img src="assets/images/ico_dewasa.png" alt="">
					</div>
					<div class="info"> 2 Wisatawan</div>
					<div class="clearfix"></div>
				</div>
				<div class="info_total">
					TOTAL
					<strong>Rp.12.000.000,-</strong>
					<span>(1 orang x Rp.2.500.000)</span>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="infowisatawan">
				<h3>Info Wisatawan</h3>
				<div class="iw">
					<div class="no">1</div>
					<div class="isi">
						<div class="jdl">Nama</div>
						<div class="isi2">Mr. Rudianto Sucipto</div>
						<div class="clearfix"></div>
						<div class="jdl">Tanggal Lahir</div>
						<div class="isi2">3 Januari 1992</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="iw">
					<div class="no">2</div>
					<div class="isi">
						<div class="jdl">Nama</div>
						<div class="isi2">Mr. Rudianto Sucipto</div>
						<div class="clearfix"></div>
						<div class="jdl">Tanggal Lahir</div>
						<div class="isi2">3 Januari 1992</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<br>
			<div class="clearfix"></div>
			<div class="line"></div>
			<div align="center">
				<a href="#" class="btn box_modal_full" alt="order_info.php">CETAK PESANAN</a>
			</div>
		</div>
	</div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
<script type='text/javascript'>
$(window).load(function(){
	var array = ["2017-05-19","2017-05-26","2017-05-12","2017-05-05"]
	  $('.pilihtanggal').datepicker({
	    beforeShowDay: function(date){
	        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
	        return [ array.indexOf(string) >= 0 ]
	    }
	});
});
</script>
</body>
</html>